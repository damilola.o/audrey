defmodule AudreyWeb.PageController do
  use AudreyWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
